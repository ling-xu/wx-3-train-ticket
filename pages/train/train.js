
Page({
  data: {
    indicatorDots:false,
    autoplay:true,
    interval:3000,
    duration:500,
    imgUrls:[
      '/images/haibao/1.jpg',
      '/images/haibao/2.jpg',
      '/images/haibao/3.jpg'
    ],
    currentTab:0,
    iconList:[
      {
        url:'../../images/icon/hcp/jsqp.jpg',
        val:'急速选票'
      },
      {
        url:'../../images/icon/hcp/zxxz.jpg',
        val:'在线选座'
      },
      {
        url:'../../images/icon/hcp/qshh.jpg',
        val:'抢手好货'
      },
      {
        url:'../../images/icon/hcp/czjd.jpg',
        val:'超值酒店'
      }
    ]
  },
  onLoad:function(options){

  },
  switchNav:function(e){
    var id=e.currentTarget.id
      this.setData({
        currentTab:id
      })
  },
  formSubmit(e){
    var startStation=e.detail.value.startStation;
    var endStation=e.detail.value.endStation;
    var date=e.detail.value.date;
    wx.navigateTo({
      url:'../trainList/trainList?startStation='+startStation+'&endStation='+endStation+'&date='+date
    })
  }
})